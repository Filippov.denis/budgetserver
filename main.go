package main

import (
	"BudgetSrv2/controllers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	"BudgetSrv2/PgRepo"
	"BudgetSrv2/models"
	"encoding/json"
	"os"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

type Configuration struct {
	WebPort          string
	DBType           string
	ConnectionString string
}

func initDB(db *gorm.DB) {
	db.AutoMigrate(&PgRepo.PgCard{})
	db.AutoMigrate(&models.Shop{})
	cardRepo := PgRepo.PgCardRepo{Db: db}
	holder := controllers.RepoHolder{CardRepo: cardRepo}
	controllers.SetRepoHolder(&holder)
}

func main() {
	//connStr := "user=postgres password=tamplier dbname=Budget sslmode=disable"
	var configuration Configuration
	filename := "config/config.json"
	file, errF := os.Open(filename)
	if errF != nil {
		log.Fatal(errF)
	}
	decoder := json.NewDecoder(file)
	errF = decoder.Decode(&configuration)
	if errF != nil {
		log.Fatal(errF)
	}

	db, err := gorm.Open(configuration.DBType, configuration.ConnectionString)
	if err != nil {
		log.Fatal(err)
	}

	println(configuration.WebPort)

	initDB(db)
	defer db.Close()

	holder := controllers.GetRepoHolder()

	cards := holder.CardRepo.GetCards()

	for _, card := range cards {
		println(card.CardName)
	}

	controllers.SetDB(db)

	router := mux.NewRouter()
	router.HandleFunc("/card", controllers.CardsGetHandler).Methods("GET")
	router.HandleFunc("/card/{id}", controllers.CardGetHandler).Methods("GET")
	router.HandleFunc("/card/{id}", controllers.CardPostHandler).Methods("POST")
	router.HandleFunc("/card", controllers.CardPutHandler).Methods("PUT")
	router.HandleFunc("/card/{id}", controllers.CardDeleteHandler).Methods("DELETE")

	router.HandleFunc("/shop", controllers.ShopsGetHandler).Methods("GET")
	http.Handle("/", router)
	//http.ListenAndServe(":8081", nil)
	http.ListenAndServeTLS(configuration.WebPort, "cert.pem", "key.pem", nil)
	println("hello")
}
